﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerBehaviour : MonoBehaviour
{
    public PlayerMovimentation playerMovimentation;
    public PlayerCombat playerCombat;
    public PlayerAnimationStatus playerAnimationStatus;
    public Animator animator;
    public float totalLife,life;
    public SimpleHealthBar healthBar;
    void Start()
    {
        life = totalLife;
    }

    void Update()
    {
        healthBar.UpdateBar( life, totalLife );
    }
    public void TakeDamage(float damage){
        life =- damage;
        life = Mathf.Clamp(life, 0, totalLife);
        animator.SetTrigger("damage");

        if(life <= 0){
            animator.SetTrigger("die");
        }
    }
    public void DealDamage(float damage, GameObject enemy){
        enemy.GetComponent<AnimalBehaviour>().TakeDamage(damage);
    }
}
