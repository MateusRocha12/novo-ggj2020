﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationStatus : MonoBehaviour
{
    public bool canMove;
    public GameObject projectile;
    public Transform firePoint;
    public void RealShoot(){
        Instantiate(projectile, firePoint.transform.position, firePoint.transform.rotation);
    }
}
