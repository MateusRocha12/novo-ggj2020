﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public bool sword,shield,bow;
    public Animator anim;
    public GameObject projectile;

    void Update()
    {
        Attack();
        Shield();
        Bow();
        anim.SetBool("sword",sword);
        anim.SetBool("shield",shield);
        anim.SetBool("bow",bow);
    }
    public void Attack(){
        sword = Input.GetButtonDown("Sword");
    }
    public void Shield(){
        shield = Input.GetButton("Shield"); 
    }
    public void Bow(){
        anim.gameObject.GetComponent<PlayerAnimationStatus>().projectile = projectile;
        bow = Input.GetButtonDown("Bow");
    }
}
