﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovimentation : MonoBehaviour
{
    public PlayerCombat playerCombat;
    public PlayerAnimationStatus playerAnimationStatus;
    public float moveSpeed;
    public GameObject model;
    public Animator anim;
    public float move;
    Vector3 direction;
    void Start(){
        
    }

    void Update(){
        direction = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
        if(playerAnimationStatus.canMove){
            Move();
        }
        anim.SetFloat("move",move);
    }

    public void Move(){
        if(Mathf.Abs(direction.x) >= Mathf.Abs(direction.z)){
            move = Mathf.Abs(direction.x);
        }else{
            move = Mathf.Abs(direction.z);
        }
        if(direction != Vector3.zero){
            Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
            model.transform.rotation = rotation;
        }
        transform.Translate(direction * (moveSpeed * Time.deltaTime),transform);
    }
    public void Dash(){

    }
    public void Atack(){
        
    }
    public void Shield(){

    }
    public void Bow(){

    }
}
