﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonerAnimationScripts : MonoBehaviour
{
    public Transform animal;
    public void Attack(){
        animal.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 30;
        animal.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(animal.gameObject.GetComponent<AnimalBehaviour>().player.position);
        // animal.gameObject.GetComponent<Rigidbody>().AddForce(animal.transform.forward*1000);
        gameObject.GetComponent<Animator>().SetBool("attack",false);
        gameObject.GetComponent<Animator>().SetBool("moving",false);
        animal.gameObject.GetComponent<AnimalBehaviour>().atacando = false;
        animal.gameObject.GetComponent<AnimalBehaviour>().state = "iddle";
        animal.gameObject.GetComponent<AnimalBehaviour>().SetStateFunctions();
    }
}
