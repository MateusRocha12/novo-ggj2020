﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalBehaviour : MonoBehaviour
{
    public float walkSpeed,runSpeed;
    public bool medroso,neutro, agressivo;
    public Animator animator;
    public float life,totalLife;
    public UnityEngine.AI.NavMeshAgent agent;
    public Transform target,player;
    public bool andando,atacando;
    public string state;
    public float distance;
    void Start()
    {
        agent.speed = walkSpeed;
        life = totalLife;
        agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
        if(state == ""){
            state = "iddle";
        }
        SetStateFunctions();
    }
    void Update(){
        StatesControl();
    }
    public void SetStateFunctions(){
        StopCoroutine("Iddle");
        StopCoroutine("Vagar");
        switch (state){
            case("iddle"):
                StartCoroutine("Iddle");
            break;
            case("vagando"):
                StartCoroutine("Vagar");
            break;
            case("perseguindo"):
                Perseguir();
            break;
            case("fugindo"):
                Fugir();
            break;
            case("atacando"):
                Atacar();
            break;
        }
    }
    public void StatesControl(){
        var col1 = Physics.OverlapSphere(transform.position, 10);
        if(col1.Length > 0){
            foreach (var item in col1){
                if(item.gameObject.CompareTag("Player")){
                    if(medroso){
                        player = item.transform;
                        state = "fugindo";
                        SetStateFunctions();
                    }else if(agressivo){
                        player = item.transform;
                        distance =  Vector3.Distance(transform.position, player.position);
                        Debug.Log("vou ver o que fazer");
                        if(distance > 5 && state != "atacando"){
                            Debug.Log("perseguir");
                            state = "perseguindo";
                            SetStateFunctions();
                        }else{
                            Debug.Log("atacar");
                            state = "atacando";
                            SetStateFunctions();
                        }
                    } 
                }else{
                    if(state == "fugindo" || state == "perseguindo"){
                        state = "iddle";
                        SetStateFunctions();
                    }
                }
            }
        }
    }
    public IEnumerator Iddle(){
        agent.speed = 0;
        animator.SetBool("moving",false);
        yield return new WaitForSeconds(5);
        state = "vagando";
        SetStateFunctions();
    }
    public IEnumerator Vagar(){
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(0,Random.RandomRange(0,359),0), Time.deltaTime * 30);
        agent.SetDestination(target.position);
        agent.speed = walkSpeed;
        animator.SetBool("moving",true);
        yield return new WaitForSeconds(3);
        state = "iddle";
        SetStateFunctions();
    }
    public void Fugir(){
        agent.speed = runSpeed;
        Vector3 dirToPlayer = transform.position - player.position;
        Vector3 newPos = transform.position + dirToPlayer;
        agent.SetDestination(newPos);
        animator.SetBool("moving",true);
    }
    public void Perseguir(){
            agent.speed = runSpeed;
            agent.SetDestination(player.position);
            animator.SetBool("moving",true);
    }
    public void Atacar(){
        if(!atacando){
            agent.speed = 0;
            atacando = true;
            animator.SetBool("attack",true);
        }
    }
    public void TakeDamage(float damage){
        life =- damage;
        life = Mathf.Clamp(life, 0, totalLife);
        animator.SetTrigger("damage");
        if(life <= 0){
            animator.SetTrigger("die");
        }
    }
    public void DealDamage(float damage, GameObject player){
        player.GetComponent<PlayerBehaviour>().TakeDamage(damage);
    }
}
